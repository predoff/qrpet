﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Text.RegularExpressions;
using System.Linq.Expressions;

using System.Reflection;
using System.ComponentModel;


using System.Collections;

namespace QRPet.Utils
{
    public static class HtmlHelperExtension
    {
        public static string Script(this HtmlHelper helper, string src)
        {
            return Script(helper, src, "text/javascript");
        }

        public static string Script(this HtmlHelper helper, string src, string type)
        {
            if (src.IndexOf("~/") != 0)
                src = "~/Scripts/" + src;


            string absoluteSrc = VirtualPathUtility.ToAbsolute(src);
            //absoluteSrc = "http://localhost:52610" + absoluteSrc;
            return string.Format("<script src=\"{0}\" type=\"{1}\"></script>",
                absoluteSrc, type);
        }

        public static string ImageLink(this HtmlHelper helper, string path, string actionName, object routeValues, object controller)
        {
            return ImageLink(helper, path, actionName, routeValues, controller, "");
        }

        public static string ImageLink(this HtmlHelper helper, string path, string actionName, object routeValues, object controller, string cssClass)
        {
            UrlHelper urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            TagBuilder builder = new TagBuilder("img");

            builder.Attributes.Add("src", urlHelper.Content(path));
            builder.MergeAttributes(new RouteValueDictionary(controller));

            if (cssClass.Length > 0)
            {
                builder.AddCssClass(cssClass);
            }

            string retorno = System.Web.Mvc.Html.LinkExtensions.ActionLink(helper, "[img]", actionName, routeValues, controller).ToHtmlString();

            return retorno.Replace("[img]", builder.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString ActionImage(this HtmlHelper helper, string action, object controller, object routeValues, string imagePath, string alt, string cssClass)
        {
            //  Adicionar Controler e Classe da imagem !!!
            UrlHelper urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            TagBuilder builder = new TagBuilder("img");

            builder.MergeAttribute("src", urlHelper.Content(imagePath));
            builder.MergeAttributes(new RouteValueDictionary(controller));
            builder.MergeAttribute("alt", alt);

            if (cssClass.Length > 0)
            {
                builder.AddCssClass(cssClass);
            }

            string imgHtml = builder.ToString(TagRenderMode.SelfClosing);

            // build the <a> tag
            var anchorBuilder = new TagBuilder("a");
            anchorBuilder.MergeAttribute("href", urlHelper.Action(action, routeValues));
            anchorBuilder.InnerHtml = imgHtml; // include the <img> tag inside
            string anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(anchorHtml);
        }

        public static string Image(this HtmlHelper helper, string path, object atributos, string cssClass)
        {
            UrlHelper urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            TagBuilder builder = new TagBuilder("img");

            builder.Attributes.Add("src", urlHelper.Content(path));
            builder.MergeAttributes(new RouteValueDictionary(atributos));

            
            if (cssClass !=null && cssClass.Length > 0)
            {
                builder.AddCssClass(cssClass);
            }
            return builder.ToString(TagRenderMode.SelfClosing);
        }

        /// Iniciando código para DropDownList com Enums e Coleções
        public static string GetInputName<TModel, TProperty>(Expression<Func<TModel, TProperty>> expression)
        {
            if (expression.Body.NodeType == ExpressionType.Call)
            {
                MethodCallExpression methodCallExpression = (MethodCallExpression)expression.Body;
                string name = GetInputName(methodCallExpression);
                return name.Substring(expression.Parameters[0].Name.Length + 1);

            }
            return expression.Body.ToString().Substring(expression.Parameters[0].Name.Length + 1);
        }

        private static string GetInputName(MethodCallExpression expression)
        {
            // p => p.Foo.Bar().Baz.ToString() => p.Foo OR throw...
            MethodCallExpression methodCallExpression = expression.Object as MethodCallExpression;
            if (methodCallExpression != null)
            {
                return GetInputName(methodCallExpression);
            }
            return expression.Object.ToString();
        }

        public static MvcHtmlString DropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable listaDeObjetos) where TModel : class
        {
            string inputName = GetInputName(expression);
            var value = htmlHelper.ViewData.Model == null
                ? default(TProperty)
                : expression.Compile()(htmlHelper.ViewData.Model);

            return htmlHelper.DropDownList(inputName, ToSelectList(typeof(TProperty),listaDeObjetos));
        }

        public static SelectList ToSelectList(Type enumType, IEnumerable listaDeObjetos)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var item in listaDeObjetos)
            {
                //FieldInfo fi = enumType.GetField(item.ToString());
                //var attribute = fi.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();
                //var title = attribute == null ? item.ToString() : ((DescriptionAttribute)attribute).Description;
                var listItem = new SelectListItem
                {
                    Value = item.ToString(),
                    Text = item.ToString(),
                    Selected = false //selectedItem == ((int)item).ToString()
                };
                items.Add(listItem);
            }

            return new SelectList(items, "Value", "Text");
        }
        /// Finalizando código para DropDownList com Enums e Coleções
    }
}
