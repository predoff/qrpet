﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace QRPet.Utils
{

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public sealed class PropertiesMustMatchAttribute : ValidationAttribute
    {
        private const string _defaultErrorMessage = "'{0}' e '{1}' não combinam.";
        private readonly object _typeId = new object();

        public PropertiesMustMatchAttribute(string originalProperty, string confirmProperty)
            : base(_defaultErrorMessage)
        {
            OriginalProperty = originalProperty;
            ConfirmProperty = confirmProperty;
        }

        public string ConfirmProperty { get; private set; }
        public string OriginalProperty { get; private set; }

        public override object TypeId
        {
            get
            {
                return _typeId;
            }
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentUICulture, ErrorMessageString,
                OriginalProperty, ConfirmProperty);
        }

        public override bool IsValid(object value)
        {
            try
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(value);
                object originalValue = properties.Find(OriginalProperty, true /* ignoreCase */).GetValue(value);
                object confirmValue = properties.Find(ConfirmProperty, true /* ignoreCase */).GetValue(value);
                return Object.Equals(originalValue, confirmValue);
            }
            catch
            {
                return false;
            }
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public sealed class ValidaCpfCnpj : ValidationAttribute
    {
        private readonly object _typeId = new object();
        public ValidaCpfCnpj()
            : base(@"^\d{3}\.\d{3}\.\d{3}\-(?<id>(\d{2}))")
        {
        }
        public override object TypeId
        {
            get
            {
                return _typeId;
            }
        }



        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;//ValidadorDeDocumentos.ValidarCnpjOuCpf(value.ToString());
            }
            else
            {
                return true;
            }
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public sealed class ValidaDataNascimento : RegularExpressionAttribute
    {
        private readonly object _typeId = new object();

        public ValidaDataNascimento()
            : base(@"^(?=\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\x20|$))|(?:2[0-8]|1\d|0?[1-9]))([-./])(?:1[012]|0?[1-9])\1(?:1[6-9]|[2-9]\d)?\d\d(?:(?=\x20\d)\x20|$))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$")
        {
        }
        public override object TypeId
        {
            get
            {
                return _typeId;
            }
        }

        public override string FormatErrorMessage(string name)
        {
            return "Data de Nascimento Inválida";
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public sealed class ValidaEmail : RegularExpressionAttribute
    {
        private readonly object _typeId = new object();

        public ValidaEmail()
            : base(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}")
        {
        }
        public override object TypeId
        {
            get
            {
                return _typeId;
            }
        }

    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public sealed class ValidaTelefone : RegularExpressionAttribute
    {
        private readonly object _typeId = new object();

        public ValidaTelefone()
            : base(@"^[(]{1}\d{2}[)]{1}\d{4}[-]{1}\d{4}$")
        {
        }
        public override object TypeId
        {
            get
            {
                return _typeId;
            }
        }


    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public sealed class ValidaNome : RegularExpressionAttribute
    {
        private readonly object _typeId = new object();

        public ValidaNome()
            : base(@"^[a-zA-Z''-'\s]{1,40}$")
        {
        }
        public override object TypeId
        {
            get
            {
                return _typeId;
            }
        }


    }
}
