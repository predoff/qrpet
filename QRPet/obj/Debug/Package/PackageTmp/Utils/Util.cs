﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QRPet.Infra.BD;

namespace QRPet.Utils
{
    public class Util
    {
        public static IDictionary<string, string> ListarSexo()
        {
            Dictionary<string, string> lista = new Dictionary<string, string>();

            lista.Add("Selecione..", "");
            lista.Add("MASCULINO", "0");
            lista.Add("FEMININO", "1");


            return lista;
        }

        public static IDictionary<string, string> ListarSexoPet()
        {
            Dictionary<string, string> lista = new Dictionary<string, string>();

            lista.Add("Selecione..", "");
            lista.Add("MACHO", "0");
            lista.Add("FÊMEA", "1");


            return lista;
        }

        public static IDictionary<string, bool> ListarRecompensa()
        {
            Dictionary<string, bool> lista = new Dictionary<string, bool>();

            lista.Add("Selecione..", false);
            lista.Add("SIM", true);
            lista.Add("NÃO", false);


            return lista;
        }

        public static IDictionary<string, string> ListarEstado()
        {
            Dictionary<string, string> lista = new Dictionary<string, string>();

            qrpetEntities _repositorio = new qrpetEntities();
            List<estado> listaEstado = (from estado in _repositorio.estado
                                        select estado).ToList();

            lista.Add("Selecione..", "");

            foreach (var item in listaEstado)
            {
                lista.Add(item.nomeestado, item.id);
            }

            return lista;
        }

        public static IDictionary<string, int> ListarCidade(string estado)
        {
            Dictionary<string, int> lista = new Dictionary<string, int>();

            qrpetEntities _repositorio = new qrpetEntities();
            List<cidade> listaCidade = (from cidade in _repositorio.cidade
                                        where cidade.estado_id == estado
                                        select cidade).ToList();

          

            foreach (var item in listaCidade)
            {
                lista.Add(item.nomecidade, item.id);
            }

            return lista;
        }
        public static IList<cidade> ListarCidades(string estado)
        {
            qrpetEntities _repositorio = new qrpetEntities();
            List<cidade> listaCidade = (from cidade in _repositorio.cidade
                                        where cidade.estado_id == estado
                                        select cidade).ToList();




            return listaCidade;
        }

        public static IDictionary<string, string> ListarTipoUsuario()
        {
            Dictionary<string, string> lista = new Dictionary<string, string>();

            lista.Add("Selecione..", "");
            lista.Add("ADMINISTRADOR", "ADMINISTRADOR");
            lista.Add("COLABORADOR", "COLABORADOR");
            lista.Add("USUARIO", "USUARIO");

            return lista;
        }

        public static IDictionary<string, string> ListarTipoPet()
        {
            Dictionary<string, string> lista = new Dictionary<string, string>();

            lista.Add("Selecione..", "");
            lista.Add("Cachorro", "Cachorro");
            lista.Add("Gato", "Gato");

            return lista;
        }
    }
}