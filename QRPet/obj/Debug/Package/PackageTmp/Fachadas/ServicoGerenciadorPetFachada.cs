﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QRPet.Servicos;
using QRPet.ViewModels;
using QRPet.Infra.BD;
using QRPet.Servicos.Implementacao;
using System.IO;

namespace QRPet.Fachadas
{
    public class ServicoGerenciadorPetFachada
    {
        IServicoGerenciadorPet _servicoPet;
        IServicoGerenciadorUsuario _servicoUsuario;

        public ServicoGerenciadorPetFachada()
        {
            _servicoPet = new ServicoGerenciadorPet();
            _servicoUsuario = new ServicoGerenciadorUsuario();
        }

        public PetVM CriarNovoPet(PetVM petCadastro, string emailUsuario, HttpPostedFileBase arquivo, string pastaFotos)
        {
            //petCadastro.Guid = Guid.NewGuid().ToString();
            petCadastro.Foto = new FotoVM();
            petCadastro.Foto.Tipo = Path.GetExtension(arquivo.FileName);
            petCadastro.Foto.Caminho = pastaFotos + petCadastro.Guid + petCadastro.Foto.Tipo;

            usuario usuario = _servicoUsuario.ObterPorEmail(emailUsuario);

            if (usuario != null)
            {
                pet pet = ConvertePetVMParaPet(petCadastro);

                pet petCadastrado = _servicoPet.Criar(pet, usuario.id);

                arquivo.SaveAs(pet.foto.caminho);

                return ConvertePetParaPetVM(petCadastrado);
            }
            else
            {
                Exception ex = new ArgumentException("Falha ao tentar cadastrar novo pet.");
                throw ex;
            }
        }

        public List<PetAdminVM> ObterListaTodosPetAdminVM()
        {
            List<pet> pets = new List<pet>();

            pets = _servicoPet.ObterTodos();

            List<PetAdminVM> listaPetAdminVM = new List<PetAdminVM>();

            foreach (var item in pets)
            {
                
                PetAdminVM petAdminVM = new PetAdminVM();
                petAdminVM.Id = item.id;
                petAdminVM.Guid = item.guid;
                petAdminVM.Nome = item.nome;
                petAdminVM.Bicho = item.bicho;
                petAdminVM.Cor = item.cor;
                petAdminVM.Raca = item.raca;
                petAdminVM.Perdido = item.perdido;
                petAdminVM.EmailDono = item.usuario.email;

                listaPetAdminVM.Add(petAdminVM);
            }

            return listaPetAdminVM;
        }

        public void ExcluirPet(string guid)
        {
            bool retorno = _servicoPet.Excluir(guid);

        }

        public PetVM EditarPet(PetVM petEditar, HttpPostedFileBase arquivo)
        {
            petEditar.Foto.Tipo = Path.GetExtension(arquivo.FileName);
            pet pet = ConvertePetVMParaPet(petEditar);

            pet petEditado = _servicoPet.Editar(pet);

            arquivo.SaveAs(pet.foto.caminho);

            return ConvertePetParaPetVM(petEditado);

        }

        public PetPerdidoVM AnunciarPerdido(PetPerdidoVM petPerdido)
        {

            pet pet = _servicoPet.ObterPorGuid(petPerdido.Guid);
            pet.datahoraperdido = petPerdido.DataPerdido;
            pet.recompensa = petPerdido.Recompensa;

            pet = _servicoPet.AnunciarPerdido(pet);

            return petPerdido;

        }

        public PetVM AnunciarEncontrado(PetVM petEncontrado)
        {
            pet pet = _servicoPet.ObterPorGuid(petEncontrado.Guid);
            pet = _servicoPet.AnunciarEncontrado(pet);

            return petEncontrado;
        }

        public List<PetListaVM> ListarPetsDoUsuario(string emailUsuario)
        {
            usuario usuario = _servicoUsuario.ObterPorEmail(emailUsuario);

            List<pet> listaPet = _servicoPet.ListarTodosPorIdUsuario(usuario.id);

            if (listaPet != null)
            {
                List<PetListaVM> listaPetVM = new List<PetListaVM>();

                foreach (var item in listaPet)
                {
                    listaPetVM.Add(ConvertePetParaPetListaVM(item));
                }

                return listaPetVM;
            }

            return null;
        }

        public List<PetListaVM> ListarPetsPerdidos()
        {

            List<pet> listaPet = _servicoPet.ListarTodosPerdidos();

            if (listaPet != null)
            {
                List<PetListaVM> listaPetVM = new List<PetListaVM>();

                foreach (var item in listaPet)
                {
                    listaPetVM.Add(ConvertePetParaPetListaVM(item));

                }

                return listaPetVM;
            }

            return null;
        }
        private pet ConvertePetVMParaPet(PetVM petVM)
        {
            try
            {
                pet pet = new pet();
                if (petVM.Guid != null)
                {
                    pet.guid = petVM.Guid.ToString();
                }
                pet.nome = petVM.Nome;
                pet.cor = petVM.Cor;
                pet.raca = petVM.Raca;
                pet.sexo = petVM.Sexo;
                pet.bicho = petVM.Bicho;
                pet.qrcode = petVM.QRCode;
                pet.descricao = petVM.Descricao;
                pet.perdido = petVM.Perdido;
                pet.foto = new foto();
                pet.foto.caminho = petVM.Foto.Caminho;
                pet.foto.nome = petVM.Foto.Nome;
                pet.foto.descricao = petVM.Foto.Descricao;
                pet.foto.tipo = petVM.Foto.Tipo;
                pet.foto.tamanholargura = petVM.Foto.TamanhoLargura;
                pet.foto.tamanhoaltura = petVM.Foto.TamanhoAltura;

                return pet;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public PetVM ObterPetVMPorGuid(string guid)
        {
            pet pet = _servicoPet.ObterPorGuid(guid);


            return ConvertePetParaPetVM(pet);
        }

        public PetPerdidoVM ObterPetPerdidoVMPorGuid(string guid)
        {
            //essa funçao so instancia um PetPerdidoVM e atribui o guid, para ser usado na página PetPerdido
            PetPerdidoVM petPerdido = new PetPerdidoVM();
            petPerdido.Guid = guid;
            

            return petPerdido;
        }


        public PetVM ObterPetVMPorGuidProcurado(string guid)
        {
            pet pet = _servicoPet.ObterPorGuid(guid);

            PetVM petVM=  ConvertePetParaPetVM(pet);
            petVM.EmailContato = pet.usuario.email;
            return petVM;
        }
        private PetListaVM ConvertePetParaPetListaVM(pet pet)
        {
            try
            {
                PetListaVM petLista = new PetListaVM();
                petLista.Nome = pet.nome;
                petLista.Guid = pet.guid;
                petLista.Id = pet.id;
                petLista.Raca = pet.raca;
                petLista.Bicho = pet.bicho;
                petLista.Perdido = pet.perdido;
                petLista.Foto = new FotoVM();
                petLista.Foto.Caminho = pet.foto.caminho;
                petLista.Foto.Nome = pet.foto.nome;
                petLista.Foto.Descricao = pet.foto.descricao;
                petLista.Foto.Tipo = pet.foto.tipo;
                petLista.Foto.TamanhoLargura = Convert.ToInt32(pet.foto.tamanholargura);
                petLista.Foto.TamanhoAltura = Convert.ToInt32(pet.foto.tamanhoaltura);
                return petLista;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private PetVM ConvertePetParaPetVM(pet pet)
        {
            try
            {
                PetVM petVM = new PetVM();
                petVM.Nome = pet.nome;
                petVM.Guid = pet.guid;
                petVM.Id = pet.id;
                petVM.Cor = pet.cor;
                petVM.Bicho = pet.bicho;
                petVM.Descricao = pet.descricao;
                petVM.QRCode = pet.qrcode;
                petVM.Raca = pet.raca;
                petVM.Sexo = pet.sexo;
                petVM.Perdido = pet.perdido;
                petVM.Recompensa = Convert.ToBoolean(pet.recompensa);
                petVM.DataPerdido = Convert.ToDateTime(pet.datahoraperdido);
                petVM.Foto = new FotoVM();
                petVM.Foto.Caminho = pet.foto.caminho;
                petVM.Foto.Nome = pet.foto.nome;
                petVM.Foto.Descricao = pet.foto.descricao;
                petVM.Foto.Tipo = pet.foto.tipo;
                petVM.Foto.TamanhoLargura = Convert.ToInt32(pet.foto.tamanholargura);
                petVM.Foto.TamanhoAltura = Convert.ToInt32(pet.foto.tamanhoaltura);

                return petVM;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}