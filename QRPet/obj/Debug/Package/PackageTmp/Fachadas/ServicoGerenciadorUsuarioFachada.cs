﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QRPet.Servicos;
using QRPet.Servicos.Implementacao;
using QRPet.ViewModels;
using QRPet.Infra.BD;

namespace QRPet.Fachadas
{
    public class ServicoGerenciadorUsuarioFachada
    {
        IServicoGerenciadorUsuario _servicoUsuario;

        public ServicoGerenciadorUsuarioFachada()
        {
            _servicoUsuario = new ServicoGerenciadorUsuario();
        }


        public void CriarNovoUsuario(UsuarioCadastroVM usuarioCadastro)
        {
            usuario usuario = ConverteUsuarioCadastroVMParaUsuario(usuarioCadastro);

            usuario usuarioCadastrado = _servicoUsuario.Criar(usuario);
        }

        public void EditarUsuario(UsuarioEditarVM usuarioEditar)
        {
            usuario usuario = ConverteUsuarioEditarVMParaUsuario(usuarioEditar);

            usuario usuarioEditado = _servicoUsuario.Editar(usuario);
        }

        public List<UsuarioAdminVM> ObterListaTodosUsuarioAdminVM()
        {
            List<usuario> usuarios = new List<usuario>();

            usuarios = _servicoUsuario.ObterTodos();

            List<UsuarioAdminVM> listaUsuariosAdminVM = new List<UsuarioAdminVM>();

            foreach (var item in usuarios)
            {
                UsuarioAdminVM userAdminVM = new UsuarioAdminVM();
                userAdminVM.Id = item.id;
                userAdminVM.Guid = item.guid;
                userAdminVM.Nome = item.nome;
                userAdminVM.Email = item.email;
                userAdminVM.Nivel = item.nivel;

                listaUsuariosAdminVM.Add(userAdminVM);
            }

            return listaUsuariosAdminVM;
        }

        public UsuarioAdminVM ObterUsuarioAdminVM(String guid)
        {


            usuario usuario = new usuario();
            usuario = _servicoUsuario.ObterPorGuid(guid);

            UsuarioAdminVM usuarioAdminVM = new UsuarioAdminVM();

            usuarioAdminVM.Id = usuario.id;
            usuarioAdminVM.Guid = usuario.guid;
            usuarioAdminVM.Nome = usuario.nome;
            usuarioAdminVM.Email = usuario.email;
            usuarioAdminVM.Nivel = usuario.nivel;


            return usuarioAdminVM;
        }

        public void EditarNivelUsuario(UsuarioAdminVM usuarioAdminVM)
        {
            usuario usuario = new usuario();
            usuario = _servicoUsuario.ObterPorGuid(usuarioAdminVM.Guid);
            usuario.nivel = usuarioAdminVM.Nivel;
            usuario = _servicoUsuario.Editar(usuario);
        }

        public void AlterarSenha(UsuarioAlterarSenhaVM usuarioAlterarSenha)
        {
            usuario usuarioSenhaAlterada = _servicoUsuario.AlterarSenha(usuarioAlterarSenha.Guid, usuarioAlterarSenha.SenhaAtual, usuarioAlterarSenha.SenhaNova);
        }

        public void ExcluirUsuario(UsuarioEditarVM usuarioEditar)
        {
            usuario usuario = _servicoUsuario.ObterPorGuid(usuarioEditar.Guid);

            _servicoUsuario.Excluir(usuario);
        }

        public UsuarioEditarVM ObterUsuarioEditarVMPorEmailESenha(UsuarioLoginVM usuarioLogin)
        {
            usuario usuario = _servicoUsuario.ObterPorEmailESenha(usuarioLogin.Email, usuarioLogin.Senha);

            return ConverteUsuarioParaUsuarioEditarVM(usuario);
        }

        public UsuarioEditarVM ObterUsuarioEditarVMPorEmail(string email)
        {
            usuario usuario = _servicoUsuario.ObterPorEmail(email);

            return ConverteUsuarioParaUsuarioEditarVM(usuario);
        }

        public UsuarioEditarVM ObterUsuarioEditarVMPorGuid(string guid)
        {
            usuario usuario = _servicoUsuario.ObterPorGuid(guid);

            return ConverteUsuarioParaUsuarioEditarVM(usuario);
        }

        public UsuarioAlterarSenhaVM ObterUsuarioAlterarSenhaVMPorGuid(string guid)
        {
            usuario usuario = _servicoUsuario.ObterPorGuid(guid);

            return ConverteUsuarioParaUsuarioAlterarSenhaVM(usuario);
        }



        public UsuarioAlterarSenhaVM ConverteUsuarioParaUsuarioAlterarSenhaVM(usuario usuario)
        {
            try
            {
                UsuarioAlterarSenhaVM usuarioSenha = new UsuarioAlterarSenhaVM();
                usuarioSenha.Id = usuario.id;
                usuarioSenha.Guid = usuario.guid;

                return usuarioSenha;
            }
            catch (Exception)
            {
                return null;
            }
            throw new NotImplementedException();
        }

        private UsuarioEditarVM ConverteUsuarioParaUsuarioEditarVM(usuario usuario)
        {
            try
            {
                UsuarioEditarVM UsuarioEditarVM = new UsuarioEditarVM();
                UsuarioEditarVM.Nome = usuario.nome;
                UsuarioEditarVM.Email = usuario.email;
                UsuarioEditarVM.Id = usuario.id;
                UsuarioEditarVM.Guid = usuario.guid;
                UsuarioEditarVM.Sexo = usuario.sexo;
                UsuarioEditarVM.Telefone = usuario.telefone;
                UsuarioEditarVM.Celular = usuario.celular;

                UsuarioEditarVM.Endereco = new EnderecoVM();
                UsuarioEditarVM.Endereco.CEP = usuario.endereco.cep;
                UsuarioEditarVM.Endereco.Rua = usuario.endereco.rua;
                UsuarioEditarVM.Endereco.Complemento = usuario.endereco.complemento;
                UsuarioEditarVM.Endereco.Bairro = usuario.endereco.bairro;
                UsuarioEditarVM.Endereco.Numero = usuario.endereco.numero;
                UsuarioEditarVM.Endereco.Cidade = usuario.endereco.cidade.id;
                UsuarioEditarVM.Endereco.Estado = usuario.endereco.cidade.estado_id;

                return UsuarioEditarVM;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private usuario ConverteUsuarioCadastroVMParaUsuario(UsuarioCadastroVM usuarioCadastro)
        {
            try
            {
                usuario usuario = new usuario();
                usuario.endereco = new endereco();

                usuario.nome = usuarioCadastro.Nome;
                usuario.email = usuarioCadastro.Email;
                usuario.sexo = usuarioCadastro.Sexo;
                usuario.telefone = usuarioCadastro.Telefone;
                usuario.celular = usuarioCadastro.Celular;
                usuario.senha = usuarioCadastro.Senha;
                usuario.endereco.cep = usuarioCadastro.Endereco.CEP;
                usuario.endereco.rua = usuarioCadastro.Endereco.Rua;
                usuario.endereco.complemento = usuarioCadastro.Endereco.Complemento;
                usuario.endereco.bairro = usuarioCadastro.Endereco.Bairro;
                usuario.endereco.numero = usuarioCadastro.Endereco.Numero;
                usuario.endereco.cidade = new cidade();
                usuario.endereco.cidade.id = usuarioCadastro.Endereco.Cidade;
                //usuario.Endereco.Estado = usuarioCadastro.Endereco.Estado;


                return usuario;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private usuario ConverteUsuarioEditarVMParaUsuario(UsuarioEditarVM usuarioEditar)
        {
            try
            {
                usuario usuario = new usuario();
                usuario.endereco = new endereco();

                usuario.guid = usuarioEditar.Guid;
                usuario.nome = usuarioEditar.Nome;
                usuario.email = usuarioEditar.Email;
                usuario.sexo = usuarioEditar.Sexo;
                usuario.telefone = usuarioEditar.Telefone;
                usuario.celular = usuarioEditar.Celular;
                usuario.endereco.cep = usuarioEditar.Endereco.CEP;
                usuario.endereco.rua = usuarioEditar.Endereco.Rua;
                usuario.endereco.complemento = usuarioEditar.Endereco.Complemento;
                usuario.endereco.bairro = usuarioEditar.Endereco.Bairro;
                usuario.endereco.numero = usuarioEditar.Endereco.Numero;
                usuario.endereco.cidade = new cidade();
                usuario.endereco.cidade.id = usuarioEditar.Endereco.Cidade;

                return usuario;
            }
            catch (Exception)
            {
                return null;
            }
        }



    }
}