﻿/*-----------------------------------------------------------------------------------*/
/*  LARGE SLIDER
/*-----------------------------------------------------------------------------------*/

$(function () {
    $('.large-slider').hover(
        function () { $('.controls').show(); },
        function () { $('.controls').hide(); }
    );

    $('.large-slides').each(function (i) {
        var p = this.parentNode;
        $(this).after('<ul class="pagination pagination-l' + i + '">').cycle({
            timeout: 0,  // milliseconds between slide transitions (0 to disable auto advance)
            speed: 1000,  // speed of the transition 
            pause: 1,     // true to enable "pause on hover"
            next: $('.next', p),
            prev: $('.prev', p),
            pager: '.pagination-l' + i
        });
    });
});


/*-----------------------------------------------------------------------------------*/
/*  MEDIUM SLIDER
/*-----------------------------------------------------------------------------------*/

$(function () {
    $('.medium-slider').hover(
        function () { $('.controls').show(); },
        function () { $('.controls').hide(); }
    );

    $('.medium-slides').each(function (i) {
        var p = this.parentNode;
        $(this).after('<ul class="pagination pagination-m' + i + '">').cycle({
            timeout: 0,  // milliseconds between slide transitions (0 to disable auto advance)
            speed: 1000,  // speed of the transition 
            pause: 1,     // true to enable "pause on hover"
            next: $('.next', p),
            prev: $('.prev', p),
            pager: '.pagination-m' + i
        });
    });
});


/*-----------------------------------------------------------------------------------*/
/*  SMALL SLIDER
/*-----------------------------------------------------------------------------------*/

$(function () {
    $('.small-slider').hover(
        function () { $('.controls').show(); },
        function () { $('.controls').hide(); }
    );



    $('.small-slides').each(function (i) {
        var p = this.parentNode;
        $(this).after('<ul class="pagination pagination-s' + i + '">').cycle({
            timeout: 0,  // milliseconds between slide transitions (0 to disable auto advance)
            speed: 1000,  // speed of the transition 
            pause: 1,     // true to enable "pause on hover"
            next: $('.next', p),
            prev: $('.prev', p),
            pager: '.pagination-s' + i
        });
    });
});


/*-----------------------------------------------------------------------------------*/
/*  PORTFOLIO SLIDER
/*-----------------------------------------------------------------------------------*/

$(function () {
    $('.portfolio-slider').hover(
        function () { $('.controls').show(); },
        function () { $('.controls').hide(); }
    );

    $('.portfolio-slides').each(function (i) {
        var p = this.parentNode;
        $(this).after('<ul class="pagination pagination' + i + '">').cycle({
            timeout: 0,  // milliseconds between slide transitions (0 to disable auto advance)
            speed: 1000,  // speed of the transition 
            pause: 1,     // true to enable "pause on hover"
            next: $('.next', p),
            prev: $('.prev', p),
            pager: '.pagination' + i
        });
    });
});

