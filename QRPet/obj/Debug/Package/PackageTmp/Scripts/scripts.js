﻿/*-----------------------------------------------------------------------------------*/
/*  TOGGLE AND TABs
/*-----------------------------------------------------------------------------------*/

$(document).ready(function () {
    //Hide the tooglebox when page load
    $(".togglebox").hide();
    //slide up and down when click over heading 2
    $("h4").click(function () {
        // slide toggle effect set to slow you can set it to fast too.
        $(this).toggleClass("active").next(".togglebox").slideToggle("slow");
        return true;
    });
});


var $j = jQuery.noConflict();    // assign the main jQuery object to $j
// perform JavaScript after the document is scriptable.
$j(function () {
    // setup ul.tabs to work as tabs for each div directly under div.panes
    $j("ul.tabs").tabs("div.panes > div", { effect: 'fade' });
});


/*-----------------------------------------------------------------------------------*/
/*  IMAGE HOVER
/*-----------------------------------------------------------------------------------*/
$(function () {
    $('.button, .flickrpress img, ul.works img, .gallery img').css("opacity", "1.0");
    $('.button, .flickrpress img, ul.works img, .gallery img').hover(function () {
        $(this).stop().animate({ opacity: 0.75 }, "fast");
    },
function () {
    $(this).stop().animate({ opacity: 1.0 }, "fast");
});
});

$(function () {
    $('.attachment-blog_thumb, #submit-button').css("opacity", "1.0");
    $('.attachment-blog_thumb, #submit-button').hover(function () {
        $(this).stop().animate({ opacity: 0.80 }, "fast");
    },
function () {
    $(this).stop().animate({ opacity: 1.0 }, "fast");
});
});


/*-----------------------------------------------------------------------------------*/
/*  GALLERY IMAGE LOAD
/*-----------------------------------------------------------------------------------*/

$(document).ready(function () {

    $("ul.gallery img").hide()
    $("ul.gallery img").each(function (i) {
        $(this).delay(i * 200).fadeIn();
    });

});