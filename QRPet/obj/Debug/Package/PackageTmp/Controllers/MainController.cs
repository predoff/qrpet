﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QRPet.ViewModels;
using QRPet.Fachadas;
using System.Web.Security;
using QRPet.Utils;

namespace QRPet.Controllers
{
    public class MainController : Controller
    {
        ServicoGerenciadorUsuarioFachada _servicoUsuario;
        ServicoGerenciadorPetFachada _servicoPet;


        public MainController()
        {
            _servicoUsuario = new ServicoGerenciadorUsuarioFachada();
            _servicoPet = new ServicoGerenciadorPetFachada();
        }



        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(UsuarioLoginVM usuario, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var retorno = _servicoUsuario.ObterUsuarioEditarVMPorEmailESenha(usuario);

                    FormsAuthentication.SetAuthCookie(usuario.Email, false);

                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception)
                {
                    ModelState.AddModelError("Falha no Login", "Email e/ou senha incorretos");
                }

            }

            // If we got this far, something failed, redisplay form
            return View(usuario);
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index");
        }

        public ActionResult Blog()
        {
            return View();
        }

        public ActionResult BlogPost()
        {
            return View();
        }

        public ActionResult Procurase()
        {
            try
            {
                List<PetListaVM> lista = _servicoPet.ListarPetsPerdidos() ;
                return View(lista);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ActionResult Images()
        {
            return View();
        }

        public ActionResult Columns()
        {
            return View();
        }

        public ActionResult Tabs()
        {
            return View();
        }

        public ActionResult Lists()
        {
            return View();
        }

        public ActionResult Sliders()
        {
            return View();
        }

        public ActionResult Typography()
        {
            return View();
        }

        public ActionResult Tables()
        {
            return View();
        }

        public ActionResult Homepage()
        {
            return View();
        }

        public ActionResult PageWithSidebar()
        {
            return View();
        }

        public ActionResult Contato()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contato(ContatoVM contato)
        {
            return View();
        }

    }
}
