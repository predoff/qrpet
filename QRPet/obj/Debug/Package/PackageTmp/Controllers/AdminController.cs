﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QRPet.Fachadas;
using QRPet.ViewModels;

namespace QRPet.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/
        ServicoGerenciadorUsuarioFachada _servicoUsuario;
        ServicoGerenciadorPetFachada _servicoPet;

        public AdminController()
        {
            _servicoUsuario = new ServicoGerenciadorUsuarioFachada();
            _servicoPet = new ServicoGerenciadorPetFachada();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AdministrarUsuarios()
        {
            try
            {
                return View(_servicoUsuario.ObterListaTodosUsuarioAdminVM());
            }
            catch(Exception)
            {
                return View( new List<UsuarioAdminVM>());
            }
        }

        public ActionResult AdministrarPets()
        {
            try
            {
                return View(_servicoPet.ObterListaTodosPetAdminVM());
            }
            catch (Exception)
            {
                return View(new List<PetAdminVM>());
            }
        }

        public ActionResult AdministrarDicas()
        {
            return View();
        }

        public ActionResult VerificarDenuncias()
        {
            return View();
        }

        [Authorize]
        public ActionResult ExcluirPet(string guid)
        {
            return View(_servicoPet.ObterPetVMPorGuid(guid));
        }

        [HttpPost]
        public ActionResult ExcluirPet(PetVM pet)
        {
            try
            {

                _servicoPet.ExcluirPet(pet.Guid);

                return RedirectToAction("ExclusaoSucesso");
            }
            catch (Exception)
            {
                return View(pet);
            }
        }

        [Authorize]
        public ActionResult AlterarNivel(string guid)
        {
            return View(_servicoUsuario.ObterUsuarioAdminVM(guid));
        }

        [HttpPost]
        public ActionResult AlterarNivel(UsuarioAdminVM usuario)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    _servicoUsuario.EditarNivelUsuario(usuario);

                    return RedirectToAction("NivelSucesso");
                }
                catch (Exception)
                {
                    return View();
                }
            }
            else
            {
                return View(usuario);
            }
        }

        public ActionResult ExclusaoSucesso()
        {
            return View();
        }

        public ActionResult NivelSucesso()
        {
            return View();
        }
    }
}
