﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QRPet.ViewModels;
using QRPet.Fachadas;
using QRPet.Utils;
using QRPet.Infra.BD;
using System.Web.Security;

namespace QRPet.Controllers
{
    public class UsuarioController : Controller
    {
        ServicoGerenciadorUsuarioFachada _servicoUsuario;

        public UsuarioController()
        {
            _servicoUsuario = new ServicoGerenciadorUsuarioFachada();
        }



        public ActionResult Cadastrar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Cadastrar(UsuarioCadastroVM usuario)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _servicoUsuario.CriarNovoUsuario(usuario);
                    return RedirectToAction("CadastroSucesso");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                    return View(usuario);
                }
            }
            else
            {
                return View(usuario);
            }
        }

        public ActionResult AtivarConta()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AtivarConta(Guid guid)
        {
            try
            {
                //TODO: Terminar ativação conta.

                UsuarioLoginVM usuario = new UsuarioLoginVM();
                return RedirectToAction("Index", "Main");
                //RedirectToAction(AtivacaoSucesso, usuario);
            }
            catch (Exception)
            {
                return View(guid);
            }
        }

        public ActionResult EnviarDica(String guid)
        {
            return View();
        }

        [HttpPost]
        public ActionResult EnviarDica(DicaVM dica)
        {
            try
            {
                return RedirectToAction("DicaSucesso");
            }
            catch (Exception)
            {
                return View(dica);
            }
        }


        public ActionResult ListaCidade(string id)
        {
            IDictionary<string, int> listaCidade = Util.ListarCidade(id);

            return Json(listaCidade.ToList(), JsonRequestBehavior.AllowGet);
        }



        public ActionResult AtivacaoSucesso()
        {
            return View();
        }

        public ActionResult SenhaSucesso()
        {
            return View();
        }

        public ActionResult EdicaoSucesso()
        {
            return View();
        }

        public ActionResult DicaSucesso()
        {
            return View();
        }

        public ActionResult ExclusaoSucesso()
        {
            return View();
        }


        [Authorize]
        public ActionResult Perfil()
        {
            UsuarioEditarVM usuario = _servicoUsuario.ObterUsuarioEditarVMPorEmail(User.Identity.Name);

            return View(usuario);
        }

        [Authorize]
        public ActionResult Editar(string guid)
        {
            return View(_servicoUsuario.ObterUsuarioEditarVMPorGuid(guid));
        }

        [HttpPost]
        public ActionResult Editar(UsuarioEditarVM usuario)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _servicoUsuario.EditarUsuario(usuario);
                    FormsAuthentication.SignOut();
                    FormsAuthentication.SetAuthCookie(usuario.Email, false);

                    return RedirectToAction("EdicaoSucesso");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                    return View(usuario);
                }
            }
            else
            {
                return View(usuario);
            }
        }

        [Authorize]
        public ActionResult AlterarSenha(string guid)
        {
            return View(_servicoUsuario.ObterUsuarioAlterarSenhaVMPorGuid(guid));
        }

        [HttpPost]
        public ActionResult AlterarSenha(UsuarioAlterarSenhaVM usuario)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _servicoUsuario.AlterarSenha(usuario);


                    return RedirectToAction("SenhaSucesso");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                    return View(usuario);
                }
            }
            else
            {
                return View(usuario);
            }
        }

        [Authorize]
        public ActionResult Excluir(string guid)
        {
            return View(_servicoUsuario.ObterUsuarioEditarVMPorGuid(guid));
        }

        [HttpPost]
        public ActionResult Excluir(UsuarioEditarVM usuario)
        {

            try
            {
                _servicoUsuario.ExcluirUsuario(usuario);
                FormsAuthentication.SignOut();

                return RedirectToAction("ExclusaoSucesso");
            }
            catch (Exception)
            {
                return View(usuario);
            }

        }

        public ActionResult CadastroSucesso()
        {
            return View();
        }
    }
}
