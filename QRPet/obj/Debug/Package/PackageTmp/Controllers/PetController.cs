﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QRPet.ViewModels;
using QRPet.Fachadas;
using QRPet.Infra.BD;
using System.IO;

namespace QRPet.Controllers
{
    public class PetController : Controller
    {
        //
        // GET: /Pet/
        ServicoGerenciadorPetFachada _servicoPet;
        ServicoGerenciadorUsuarioFachada _servicoUsuario;


        public PetController()
        {
            _servicoPet = new ServicoGerenciadorPetFachada();
            _servicoUsuario = new ServicoGerenciadorUsuarioFachada();
        }

        [Authorize]
        public ActionResult Index()
        {
            try
            {
                List<PetListaVM> lista = _servicoPet.ListarPetsDoUsuario(User.Identity.Name);
                return View(lista);
            }
            catch (Exception)
            {
                return null;
            }

        }

        [Authorize]
        public ActionResult Cadastrar()
        {
            PetVM pet = new PetVM();
            pet.Guid = Guid.NewGuid().ToString();
            return View(pet);
        }

        [HttpPost]
        public ActionResult Cadastrar(PetVM pet, HttpPostedFileBase arquivo)
        {
            if (ModelState.IsValid && arquivo != null)
            {
                try
                {
                    string caminho = Path.Combine(Server.MapPath("~/Content/images/pets/"));
                    PetVM petCadastrado = _servicoPet.CriarNovoPet(pet, User.Identity.Name, arquivo, caminho);

                    return RedirectToAction("CadastroSucesso");


                }
                catch (Exception)
                {
                    return View(pet);
                }
            }
            else if (arquivo == null)
            {
                ModelState.AddModelError("", "Selecione Foto");

                return View(pet);
            }
            else
            {
                return View(pet);
            }
        }

        [Authorize]
        public ActionResult Visualizar(string guid)
        {
            return View(_servicoPet.ObterPetVMPorGuid(guid));
        }

        [Authorize]
        public ActionResult Editar(string guid)
        {
            return View(_servicoPet.ObterPetVMPorGuid(guid));
        }

        [HttpPost]
        public ActionResult Editar(PetVM pet, HttpPostedFileBase arquivo)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    pet.Foto = new FotoVM();

                    pet.Foto.Caminho = Path.Combine(Server.MapPath("~/Content/images/pets/"));
                    PetVM petCadastrado = _servicoPet.EditarPet(pet, arquivo);

                    return RedirectToAction("EdicaoSucesso");
                }
                catch (Exception)
                {
                    return View(pet);
                }
            }
            else
            {
                return View(pet);
            }
        }

        public ActionResult Perfil(string guid)
        {

            return View(_servicoPet.ObterPetVMPorGuidProcurado(guid));
        }

        [Authorize]
        public ActionResult Excluir(string guid)
        {
            return View(_servicoPet.ObterPetVMPorGuid(guid));
        }

        [HttpPost]
        public ActionResult Excluir(PetVM pet)
        {
            try
            {

                _servicoPet.ExcluirPet(pet.Guid);

                return RedirectToAction("ExclusaoSucesso");
            }
            catch (Exception)
            {
                return View(pet);
            }
        }


        [Authorize]
        public ActionResult PetPerdido(string guid)
        {
            return View(_servicoPet.ObterPetPerdidoVMPorGuid(guid));
        }

        [HttpPost]
        public ActionResult PetPerdido(PetPerdidoVM pet)
        {

            try
            {

                PetPerdidoVM petCadastrado = _servicoPet.AnunciarPerdido(pet);

                return RedirectToAction("PerdidoSucesso");
            }
            catch (Exception)
            {
                return View(pet);
            }



        }

        [Authorize]
        public ActionResult PetEncontrado(string guid)
        {
            return View(_servicoPet.ObterPetVMPorGuid(guid));
        }

        [HttpPost]
        public ActionResult PetEncontrado(PetVM pet)
        {
            try
            {

                PetVM petCadastrado = _servicoPet.AnunciarEncontrado(pet);

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View(pet);
            }


        }


        public ActionResult EdicaoSucesso()
        {
            return View();
        }

        public ActionResult ExclusaoSucesso()
        {
            return View();
        }
        public ActionResult PerdidoSucesso()
        {
            return View();
        }

        public ActionResult CadastroSucesso()
        {
            return View();
        }


        //public ActionResult Upload(Guid guid)
        //{
        //    PetVM pet = _servicoPet.ObterPetVMPorGuid(guid);
        //    return View(pet);
        //}

        //[HttpPost]
        //public ActionResult Upload(Guid Guid, HttpPostedFileBase arquivo)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            string extensao = Path.GetExtension(arquivo.FileName);
        //            string path = Path.Combine(Server.MapPath("~/Content/images/pets/"), Guid +  extensao);
        //            arquivo.SaveAs(path);
        //            //Salva o cracha em algum luga
        //            //PetVM pet = _servicoPet.ObterPetVMPorGuid(guid);
        //            //_servicoPet.CriarNovoPet(pet, User.Identity.Name);
        //            return RedirectToAction("Index");
        //        }
        //        catch (Exception)
        //        {
        //            return View();
        //        }
        //    }
        //    else
        //    {
        //        return View();
        //    }
        //}

    }

}

