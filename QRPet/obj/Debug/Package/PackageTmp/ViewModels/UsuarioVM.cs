﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using QRPet.Utils;
using System.ComponentModel.DataAnnotations;

namespace QRPet.ViewModels
{

    public class UsuarioLoginVM
    {
        [Required(ErrorMessage = "Insira o Email")]
        [DisplayName("Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Insira a Senha")]
        [DisplayName("Senha")]
        [PasswordPropertyText(true)]
        [DataType(DataType.Password)]
        public string Senha { get; set; }
    }

    public class UsuarioCadastroVM
    {
        [Required(ErrorMessage = "Insira o Nome")]
        [DisplayName("Nome Completo")]
        [StringLength(150, ErrorMessage = "O campo Nome permite no máximo 150 caracteres!")]
        [ValidaNome(ErrorMessage = "Números e caracteres especiais não são permitidos no campo Nome.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Insira o Email")]
        [DisplayName("Email")]
        [ValidaEmail(ErrorMessage = "Email inválido")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Insira a Senha")]
        [DisplayName("Senha")]
        [PasswordPropertyText(true)]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [DisplayName("Sexo")]
        public string Sexo { get; set; }

        [DisplayName("Nivel")]
        public string Nivel { get; set; }

        [ValidaTelefone(ErrorMessage = "Telefone inválido")]
        [DisplayName("Telefone")]
        public string Telefone { get; set; }

        [ValidaTelefone(ErrorMessage = "Celular inválido")]
        [DisplayName("Celular")]
        public string Celular { get; set; }



        [DisplayName("Endereco")]
        public EnderecoVM Endereco { get; set; }

    }

    public class UsuarioAdminVM
    {
        [HiddenInput]
        public long Id { get; set; }

        [HiddenInput]
        public string Guid { get; set; }

        [DisplayName("Nome Completo")]
        public string Nome { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Selecione o Nível")]
        [DisplayName("Nivel")]
        public string Nivel { get; set; }

    }

    public class UsuarioEditarVM
    {
        [HiddenInput]
        public long Id { get; set; }

        [HiddenInput]
        public string Guid { get; set; }

        [Required(ErrorMessage = "Insira o Nome")]
        [DisplayName("Nome")]
        [StringLength(150, ErrorMessage = "O campo Nome permite no máximo 150 caracteres!")]
        [ValidaNome(ErrorMessage = "Números e caracteres especiais não são permitidos no campo Nome.")]
        public string Nome { get; set; }

        [DisplayName("Sexo")]
        public string Sexo { get; set; }


        [DisplayName("Nivel")]
        public string Nivel { get; set; }

        [ValidaTelefone(ErrorMessage = "Telefone inválido")]
        [DisplayName("Telefone")]
        public string Telefone { get; set; }

        [ValidaTelefone(ErrorMessage = "Celular inválido")]
        [DisplayName("Celular")]
        public string Celular { get; set; }


        [Required(ErrorMessage = "Insira o Email")]
        [ValidaEmail(ErrorMessage = "Email inválido")]
        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Endereco")]
        public EnderecoVM Endereco { get; set; }

    }

    public class EnderecoVM
    {
        [DisplayName("CEP")]
        public string CEP { get; set; }

        [DisplayName("Rua")]
        public string Rua { get; set; }

        [DisplayName("Bairro")]
        public string Bairro { get; set; }

        [DisplayName("Número")]
        public string Numero { get; set; }

        [DisplayName("Complemento")]
        public string Complemento { get; set; }

        [Required(ErrorMessage = "Insira a Cidade")]
        [DisplayName("Cidade")]
        public int Cidade { get; set; }

        [Required(ErrorMessage = "Insira o Estado")]
        [DisplayName("Estado")]
        public string Estado { get; set; }

    }

    [PropertiesMustMatch("SenhaNova", "SenhaNovaConf", ErrorMessage = "A senha e confirmação de nova senha devem ser iguais.")]
    public class UsuarioAlterarSenhaVM
    {
        [HiddenInput]
        public long Id { get; set; }

        [HiddenInput]
        public string Guid { get; set; }

        [Required(ErrorMessage = "Insira a Senha Atual")]
        [DisplayName("Senha Atual")]
        [PasswordPropertyText(true)]
        [DataType(DataType.Password)]
        public string SenhaAtual { get; set; }


        [Required(ErrorMessage = "Insira a Senha Nova")]
        [DisplayName("Senha Nova")]
        [PasswordPropertyText(true)]
        [DataType(DataType.Password)]
        public string SenhaNova { get; set; }


        [Required(ErrorMessage = "Insira a Confirmação de Senha")]
        [DisplayName("Confirmação Senha Nova")]
        [PasswordPropertyText(true)]
        [DataType(DataType.Password)]
        public string SenhaNovaConf { get; set; }
    }

    public class DicaVM
    {

        [HiddenInput]
        public long Id { get; set; }

        [HiddenInput]
        public string Guid { get; set; }

        [DisplayName("Título")]
        public string Titulo { get; set; }

        [DisplayName("Texto")]
        public string Texto { get; set; }

        [DisplayName("Usuario")]
        public string GuidUsuario { get; set; }
    }
}