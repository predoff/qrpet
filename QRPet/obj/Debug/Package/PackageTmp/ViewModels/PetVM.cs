﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using QRPet.Utils;

namespace QRPet.ViewModels
{
    public class PetVM
    {
        [HiddenInput]
        public long Id { get; set; }

        [HiddenInput]
        public string Guid { get; set; }

        [DisplayName("Nome")]
        [Required(ErrorMessage = "Insira o Nome")]
        [StringLength(100, ErrorMessage = "O campo Nome permite no máximo 100 caracteres!")]
        [ValidaNome(ErrorMessage = "Números e caracteres especiais não são permitidos no campo Nome.")]
        public string Nome { get; set; }

        [DisplayName("Sexo")]
        [Required(ErrorMessage = "Selecione o Sexo")]
        public string Sexo { get; set; }

        [DisplayName("Bicho")]
        [Required(ErrorMessage = "Insira o Bicho")]
        public string Bicho { get; set; }

        [DisplayName("Raça")]
        [Required(ErrorMessage = "Insira a Raça")]
        [StringLength(50, ErrorMessage = "O campo Raça permite no máximo 50 caracteres!")]
        public string Raca { get; set; }

        [DisplayName("Cor")]
        [Required(ErrorMessage = "Insira a Cor")]
        [StringLength(50, ErrorMessage = "O campo Cor permite no máximo 50 caracteres!")]
        public string Cor { get; set; }

        [DisplayName("Idade")]
        public string Idade { get; set; }

        [DisplayName("Descrição")]
        [StringLength(150, ErrorMessage = "O campo Descrição permite no máximo 150 caracteres!")]
        public string Descricao { get; set; }

        [DisplayName("QRCode")]
        [Required(ErrorMessage = "Insira o texto do QRCode")]
        public string QRCode { get; set; }

        [DisplayName("Foto")]
        public FotoVM Foto { get; set; }

        [DisplayName("Recompensa")]
        public bool Recompensa { get; set; }

        [DisplayName("Data de Desaparecimento")]
        public DateTime DataPerdido { get; set; }

        [DisplayName("Perdido")]
        public bool Perdido { get; set; }

        [DisplayName("Email Contato")]
        public string EmailContato { get; set; }
    }

    public class PetListaVM
    {
        [HiddenInput]
        public long Id { get; set; }

        [HiddenInput]
        public string Guid { get; set; }

        [DisplayName("Nome")]
        public string Nome { get; set; }

        [DisplayName("Bixo")]
        public string Bicho { get; set; }

        [DisplayName("Raça")]
        public string Raca { get; set; }

        [DisplayName("Foto")]
        public FotoVM Foto { get; set; }

        [DisplayName("Perdido")]
        public bool Perdido { get; set; }

     
    

    }

    public class PetAdminVM
    {
        [HiddenInput]
        public long Id { get; set; }

        [HiddenInput]
        public string Guid { get; set; }

        [DisplayName("Nome")]
        public string Nome { get; set; }

        [DisplayName("Bixo")]
        public string Bicho { get; set; }

        [DisplayName("Raça")]
        public string Raca { get; set; }

        [DisplayName("Cor")]
        public string Cor { get; set; }

        [DisplayName("Email Dono")]
        public string EmailDono { get; set; }

        [DisplayName("Perdido")]
        public bool Perdido { get; set; }
    }

    public class PetPerdidoVM
    {
        [HiddenInput]
        public long Id { get; set; }

        [HiddenInput]
        public string Guid { get; set; }

        [Required(ErrorMessage = "Selecione a Recompensa")]
        [DisplayName("Recompensa")]
        public bool Recompensa { get; set; }

        [Required(ErrorMessage = "Insira a Data de Desaparecimento")]
        [DisplayName("Data de Desaparecimento")]
        public DateTime DataPerdido { get; set; }
    }
    public class FotoVM
    {
        [HiddenInput]
        public long Id { get; set; }

        [DisplayName("Nome")]
        public string Nome { get; set; }

        [DisplayName("Descrição")]
        public string Descricao { get; set; }

        [DisplayName("Tipo")]
        public string Tipo { get; set; }

        [DisplayName("Caminho")]
        public string Caminho { get; set; }

        [DisplayName("Largura")]
        public int TamanhoLargura { get; set; }

        [DisplayName("Altura")]
        public int TamanhoAltura { get; set; }



    }

}