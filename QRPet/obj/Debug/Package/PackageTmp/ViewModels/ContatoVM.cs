﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace QRPet.ViewModels
{
    public class ContatoVM
    {
        [Required(ErrorMessage = "Insira o Nome")]
        [DisplayName("Nome")]
        public string Nome { get; set; }


        [Required(ErrorMessage = "Insira o Email")]
        [DisplayName("Email")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Insira o Assunto")]
        [DisplayName("Assunto")]
        public string Assunto { get; set; }


        [Required(ErrorMessage = "Insira o Mensagem")]
        [DisplayName("Mensagem")]
        public string Mensagem { get; set; }
    }
}