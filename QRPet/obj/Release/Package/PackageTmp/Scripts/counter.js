﻿<script type="text/javascript" src="http://www.google.com/jsapi"></script>  
  
<script type="text/javascript" language="javascript">  
google.load("jquery", "1.4.2");  
  
var characterLimit = 150;  
  
google.setOnLoadCallback(function(){  
  
    $('#remainingCharacters').html(characterLimit);  
  
    $('#myTextarea').bind('keyup', function(){  
        var charactersUsed = $(this).val().length;  
  
        if(charactersUsed > characterLimit){  
            charactersUsed = characterLimit;  
            $(this).val($(this).val().substr(0, characterLimit));  
            $(this).scrollTop($(this)[0].scrollHeight);  
        }  
  
        var charactersRemaining = characterLimit - charactersUsed;  
  
        $('#remainingCharacters').html(charactersRemaining);  
    });  
});  
</script>  
