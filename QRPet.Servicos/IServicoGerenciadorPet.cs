﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QRPet.Infra.BD;

namespace QRPet.Servicos
{
    public interface IServicoGerenciadorPet
    {
        pet Criar(pet pet, long idUsuario);

        List<pet> ListarTodosPorIdUsuario(long idUsuario);

        List<pet> ObterTodos();

        List<pet> ListarTodosPerdidos();

        pet ObterPorGuid(string guid);

        bool Excluir(string guid);

        pet Editar(pet pet);

        pet AnunciarPerdido(pet pet);

        pet AnunciarEncontrado(pet pet);
    }
}
