﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QRPet.Infra.BD;

namespace QRPet.Servicos
{
    public interface IServicoGerenciadorUsuario
    {
        usuario Criar(usuario usuario);

        usuario ObterPorEmailESenha(string email, string senha);

        usuario ObterPorEmail(string email);

        List<usuario> ObterTodos();

        usuario ObterPorGuid(string guid);

        bool Excluir(usuario usuario);

        usuario Editar(usuario usuario);

        usuario AlterarSenha(string guid, string senhaAtual, string novaSenha);

        dica EnviarDica(dica dica, string guidUsuario);

        List<dica> ObterDicas();

        dica ValidarDica(dica dica);

        bool ExcluirDica(dica dica);

        dica ObterDicaPorGuid(string guid);
    }
}
