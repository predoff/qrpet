﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QRPet.Infra.Repositorio;
using QRPet.Infra.BD;

namespace QRPet.Servicos.Implementacao
{
    public class ServicoGerenciadorUsuario : IServicoGerenciadorUsuario
    {
        RepositorioUsuario _repositorioUsuario;

        public ServicoGerenciadorUsuario()
        {
            _repositorioUsuario = new RepositorioUsuario();
        }

        ~ServicoGerenciadorUsuario()
        {
            _repositorioUsuario = null;
        }


        public usuario Criar(usuario usuario)
        {
            usuario usuarioExiste = _repositorioUsuario.ObterPorEmail(usuario.email);

            if (usuarioExiste == null)
            {
                usuarioExiste = new usuario();
                usuarioExiste.guid = Guid.NewGuid().ToString();
                usuarioExiste.nome = usuario.nome;
                usuarioExiste.email = usuario.email;
                usuarioExiste.nivel = "USUARIO";
                usuarioExiste.senha = Criptografia.Encriptar(usuario.senha);
                usuarioExiste.datahoracadastro = DateTime.Now;
                usuarioExiste.ativado = false;
                usuarioExiste.sexo = usuario.sexo;
                if (usuario.telefone != null)
                {
                    usuarioExiste.telefone = usuario.telefone.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
                }

                if (usuario.celular != null)
                {
                    usuarioExiste.celular = usuario.celular.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
                }

                //if (usuario.CPF != null)
                //{
                //    usuarioExiste.CPF = usuario.CPF.Replace(".", "").Replace("-", "").Replace("-", "").Replace("/", "").Replace("\\", "");
                //}

                usuarioExiste.endereco = new endereco();
                usuarioExiste.endereco.guid = Guid.NewGuid().ToString();
                usuarioExiste.endereco.rua = usuario.endereco.rua;
                usuarioExiste.endereco.bairro = usuario.endereco.bairro;
                usuarioExiste.endereco.complemento = usuario.endereco.complemento;
                usuarioExiste.endereco.cep = usuario.endereco.cep;
                usuarioExiste.endereco.numero = usuario.endereco.numero;

                usuarioExiste.endereco.cidade = _repositorioUsuario.ObterCidadePorId(usuario.endereco.cidade.id);
                //usuarioExiste.Endereco.Cidade = usuario.Endereco.Cidade;
                //usuarioExiste.Endereco.Estado = usuario.Endereco.Estado;

                //EnviaEmail.Valida(usuario);
                if (_repositorioUsuario.Criar(usuarioExiste))
                {

                    return usuarioExiste;
                }
                else
                {
                    Exception ex = new ArgumentException("Falha ao tentar cadastrar novo usuário.");
                    throw ex;
                }
            }
            else
            {
                Exception ex = new ArgumentException("Já existe um usuário com o mesmo email");
                throw ex;
            }

        }


        public usuario ObterPorEmailESenha(string email, string senha)
        {
            try
            {
                usuario usuario = _repositorioUsuario.ObterPorEmailESenha(email, Criptografia.Encriptar(senha));

                if (usuario == null)
                {
                    NaoLocalizadoException ex = new NaoLocalizadoException("Erro no login. Verifique o Email e Senha");
                    throw ex;
                }

                return usuario;
            }
            catch (NaoLocalizadoException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public usuario ObterPorEmail(string email)
        {
            try
            {
                usuario usuario = _repositorioUsuario.ObterPorEmail(email);

                if (usuario == null)
                {
                    NaoLocalizadoException ex = new NaoLocalizadoException("Usuário não localizado.");
                    throw ex;
                }

                return usuario;
            }
            catch (NaoLocalizadoException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public usuario ObterPorGuid(string guid)
        {
            try
            {
                usuario usuario = _repositorioUsuario.ObterPorGuid(guid);

                if (usuario == null)
                {
                    NaoLocalizadoException ex = new NaoLocalizadoException("Usuário não localizado.");
                    throw ex;
                }

                return usuario;
            }
            catch (NaoLocalizadoException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public bool Excluir(usuario usuario)
        {
            usuario usuarioExiste = _repositorioUsuario.ObterPorGuid(usuario.guid);

            if (usuarioExiste != null)
            {

                if (_repositorioUsuario.Excluir(usuarioExiste))
                {
                    return true;
                }
                else
                {
                    Exception ex = new ArgumentException("Falha ao tentar excluir usuário.");
                    throw ex;
                }

            }
            else
            {
                Exception ex = new ArgumentException("Não foi possível excluir usuário. Tente novamente mais tarde.");
                throw ex;
            }
        }

        public usuario Editar(usuario usuarioEditado)
        {
            usuario usuarioExiste = _repositorioUsuario.ObterPorGuid(usuarioEditado.guid);
            usuario usuarioEmail = _repositorioUsuario.ObterPorEmail(usuarioEditado.email);

            if (usuarioExiste != null)
            {
                if (usuarioEmail == null || usuarioExiste.guid == usuarioEmail.guid)
                {
                    usuarioExiste.nome = usuarioEditado.nome;
                    usuarioExiste.email = usuarioEditado.email;
                    usuarioExiste.ativado = false;
                    usuarioExiste.sexo = usuarioEditado.sexo;

                    if (usuarioEditado.telefone != null)
                    {
                        usuarioExiste.telefone = usuarioEditado.telefone.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
                    }
                    if (usuarioEditado.celular != null)
                    {
                        usuarioExiste.celular = usuarioEditado.celular.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
                    }

                    //usuarioExiste.endereco = new endereco();
                    //usuarioEditado.enderecoReference.Load();

                    usuarioExiste.endereco.rua = usuarioEditado.endereco.rua;
                    usuarioExiste.endereco.bairro = usuarioEditado.endereco.bairro;
                    usuarioExiste.endereco.complemento = usuarioEditado.endereco.complemento;
                    usuarioExiste.endereco.cep = usuarioEditado.endereco.cep;
                    usuarioExiste.endereco.numero = usuarioEditado.endereco.numero;

                    //usuarioExiste.endereco.cidade = new cidade();
                    usuarioExiste.endereco.cidade = _repositorioUsuario.ObterCidadePorId(usuarioEditado.endereco.cidade.id);

                    if (_repositorioUsuario.Editar(usuarioExiste))
                    {

                        return usuarioExiste;
                    }
                    else
                    {
                        Exception ex = new ArgumentException("Falha ao tentar salvar dados do usuário.");
                        throw ex;
                    }
                }
                else
                {
                    Exception ex = new ArgumentException("Email já cadastrado por outro usuário.");
                    throw ex;
                }
            }
            else
            {
                Exception ex = new ArgumentException("Não foi possível alterar usuário. Tente novamente mais tarde.");
                throw ex;
            }
        }


        public usuario AlterarSenha(string guid, string senhaAtual, string novaSenha)
        {
            usuario usuarioExiste = _repositorioUsuario.ObterPorGuid(guid);

            if (usuarioExiste != null)
            {
                if (usuarioExiste.senha == Criptografia.Encriptar(senhaAtual))
                {
                    usuarioExiste.senha = Criptografia.Encriptar(novaSenha);


                    if (_repositorioUsuario.Editar(usuarioExiste))
                    {

                        return usuarioExiste;
                    }
                    else
                    {
                        Exception ex = new ArgumentException("Falha ao tentar salvar nova senha do usuário.");
                        throw ex;
                    }
                }
                else
                {
                    Exception ex = new ArgumentException("Senha atual incorreta.");
                    throw ex;
                }

            }
            else
            {
                Exception ex = new ArgumentException("Não foi possível alterar usuário. Tente novamente mais tarde.");
                throw ex;
            }
        }


        public List<usuario> ObterTodos()
        {
            try
            {
                List<usuario> usuarios = _repositorioUsuario.ObterTodos();

                if (usuarios == null)
                {
                    NaoLocalizadoException ex = new NaoLocalizadoException("Usuários não localizados.");
                    throw ex;
                }

                return usuarios;
            }
            catch (NaoLocalizadoException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public dica EnviarDica(dica dica, string guidUsuario)
        {
            try
            {
                dica.guid = Guid.NewGuid().ToString();
                dica.dataenviada = DateTime.Now;
                dica.status = "EM ESPERA";
                usuario usuario = _repositorioUsuario.ObterPorGuid(guidUsuario);

                dica.usuario_id = usuario.id;

                _repositorioUsuario.EnviarDica(dica);

                return dica;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<dica> ObterDicas()
        {
            try
            {
                List<dica> dicas = _repositorioUsuario.ObterDicas();

                if (dicas == null)
                {
                    NaoLocalizadoException ex = new NaoLocalizadoException("Dicas não localizadss.");
                    throw ex;
                }

                return dicas;
            }
            catch (NaoLocalizadoException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public dica ValidarDica(dica dica)
        {
            try
            {
                dica.status = "APROVADA";

                _repositorioUsuario.ValidarDica(dica);

                return dica;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ExcluirDica(dica dica)
        {
            try
            {

                return _repositorioUsuario.ExcluirDica(dica);


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public dica ObterDicaPorGuid(string guid)
        {
            try
            {
                dica dica = _repositorioUsuario.ObterDicaPorGuid(guid);

                if (dica == null)
                {
                    NaoLocalizadoException ex = new NaoLocalizadoException("Dica não localizada.");
                    throw ex;
                }

                return dica;
            }
            catch (NaoLocalizadoException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
