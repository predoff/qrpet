﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QRPet.Infra.BD;
using QRPet.Infra.Repositorio;
using System.IO;
using QRPet.Servicos;

namespace QRPet.Servicos.Implementacao
{
    public class ServicoGerenciadorPet : IServicoGerenciadorPet
    {
        RepositorioUsuario _repositorioUsuario;
        RepositorioPet _repositorioPet;

        public ServicoGerenciadorPet()
        {
            _repositorioUsuario = new RepositorioUsuario();
            _repositorioPet = new RepositorioPet();
        }

        public pet Criar(pet pet, long idUsuario)
        {
            usuario usuario = _repositorioUsuario.ObterPorId(idUsuario);

            if (usuario != null)
            {
                pet.usuario_id = idUsuario;
                pet.perdido = false;
                pet.foto.caminho = pet.foto.caminho;
                pet.foto.nome = pet.guid.ToString();

                if (_repositorioPet.Criar(pet))
                {
                    return pet;
                }
                else
                {
                    Exception ex = new ArgumentException("Falha ao tentar cadastrar novo pet.");
                    throw ex;
                }
            }
            else
            {
                Exception ex = new ArgumentException("Falha ao tentar cadastrar novo pet.");
                throw ex;
            }
        }


        public pet ObterPorGuid(string guid)
        {
            try
            {
                pet pet = _repositorioPet.ObterPorGuid(guid);

                if (pet == null)
                {
                    NaoLocalizadoException ex = new NaoLocalizadoException("pet não localizado.");
                    throw ex;
                }

                return pet;
            }
            catch (NaoLocalizadoException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public bool Excluir(string guid)
        {
            try
            {
                pet pet = _repositorioPet.ObterPorGuid(guid);

                return (_repositorioPet.Excluir(pet));
            }
            catch (Exception)
            {
                return false;
            }
        }


        public pet Editar(pet pet)
        {
            pet petAlterado = _repositorioPet.ObterPorGuid(pet.guid);

            if (petAlterado != null)
            {
                petAlterado.nome = pet.nome;
                petAlterado.descricao = pet.descricao;
                petAlterado.qrcode = pet.qrcode;
                petAlterado.raca = pet.raca;
                petAlterado.sexo = pet.sexo;
                petAlterado.cor = pet.cor;
                petAlterado.bicho = pet.bicho;
                petAlterado.foto.tipo = pet.foto.tipo;
                pet.foto.caminho = pet.foto.caminho + pet.guid.ToString() + pet.foto.tipo;

                if (_repositorioPet.Editar(petAlterado))
                {
                    return petAlterado;
                }
                else
                {
                    Exception ex = new ArgumentException("Falha ao tentar editar pet.");
                    throw ex;
                }
            }
            else
            {
                Exception ex = new ArgumentException("Falha ao tentar editar pet.");
                throw ex;
            }
        }



        public pet AnunciarPerdido(pet pet)
        {
            pet petPerdido = _repositorioPet.ObterPorGuid(pet.guid);

            if (petPerdido != null)
            {
                petPerdido.perdido = true;
                petPerdido.datahoraperdido = pet.datahoraperdido;
                petPerdido.recompensa = pet.recompensa;

                if (_repositorioPet.Editar(petPerdido))
                {
                    return petPerdido;
                }
                else
                {
                    Exception ex = new ArgumentException("Falha ao tentar executar ação.");
                    throw ex;
                }
            }
            else
            {

                Exception ex = new ArgumentException("Falha ao tentar executar ação.");
                throw ex;
            }
        }

        public pet AnunciarEncontrado(pet pet)
        {
            pet petPerdido = _repositorioPet.ObterPorGuid(pet.guid);

            if (petPerdido != null)
            {
                petPerdido.perdido = false;


                if (_repositorioPet.Editar(petPerdido))
                {
                    return petPerdido;
                }
                else
                {
                    Exception ex = new ArgumentException("Falha ao tentar executar ação.");
                    throw ex;
                }
            }
            else
            {

                Exception ex = new ArgumentException("Falha ao tentar executar ação.");
                throw ex;
            }
        }


        public List<pet> ListarTodosPorIdUsuario(long idUsuario)
        {
            List<pet> lista = _repositorioPet.ListarTodosPorUsuario(idUsuario);

            return lista;
        }

        public List<pet> ListarTodosPerdidos()
        {
            List<pet> lista = _repositorioPet.ListarTodosPerdidos();

            return lista;
        }


        public List<pet> ObterTodos()
        {
            try
            {
                List<pet> pet = _repositorioPet.ObterTodos();

                if (pet == null)
                {
                    NaoLocalizadoException ex = new NaoLocalizadoException("pets não localizados.");
                    throw ex;
                }

                return pet;
            }
            catch (NaoLocalizadoException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
