﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QRPet.Infra.BD;

namespace QRPet.Infra.Repositorio
{
    public class RepositorioPet
    {
        qrpetEntities _repositorio;

        public RepositorioPet()
        {
            _repositorio = new qrpetEntities();
        }

        public pet ObterPorId(long id)
        {
            try
            {
                pet pet = (from user in _repositorio.pet
                                   where user.id == id
                                   select user).FirstOrDefault();

                return pet;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public pet ObterPorGuid(string guid)
        {
            try
            {
                pet pet = (from user in _repositorio.pet
                           where user.guid == guid
                           select user).FirstOrDefault();

                return pet;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool Criar(pet pet)
        {
            try
            {
                //pet.UsuarioReference.Value = usuario;

                _repositorio.AddTopet(pet);

                _repositorio.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Editar(pet pet)
        {
            try
            {
                _repositorio.ApplyCurrentValues(pet.EntityKey.EntitySetName, pet);
                _repositorio.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool Excluir(pet pet)
        {
            try 
            {
                foto foto = (from f in _repositorio.foto
                             where f.id == pet.foto_id
                             select f).FirstOrDefault();

                _repositorio.DeleteObject(foto);
                _repositorio.DeleteObject(pet);
                _repositorio.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<pet> ListarTodosPorUsuario(long idUsuario)
        {
            try
            {
                List<pet> lista = (from pet in _repositorio.pet
                                   where pet.usuario_id == idUsuario
                                   select pet).ToList();

                return lista;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<pet> ListarTodosPerdidos()
        {
            try
            {
                List<pet> lista = (from pet in _repositorio.pet
                                   where pet.perdido == true
                                   select pet).ToList();

                return lista;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<pet> ObterTodos()
        {
            try
            {
                List<pet> listaPet = (from pets in _repositorio.pet
                                               select pets).ToList();

                return listaPet;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
