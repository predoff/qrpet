﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QRPet.Infra.BD;

namespace QRPet.Infra.Repositorio
{
    public class RepositorioUsuario
    {
        qrpetEntities _repositorio;

        public RepositorioUsuario()
        {
            _repositorio = new qrpetEntities();
        }

        public usuario ObterPorId(long id)
        {
            try
            {
                usuario usuario = (from user in _repositorio.usuario
                                   where user.id == id
                                   select user).FirstOrDefault();

                return usuario;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public usuario ObterPorGuid(string guid)
        {
            try
            {
                usuario usuario = (from user in _repositorio.usuario
                                   where user.guid == guid
                                   select user).FirstOrDefault();

                

                return usuario;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool Criar(usuario usuario)
        {
            try
            {
                _repositorio.AddTousuario(usuario);
                
                _repositorio.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<usuario> ListarTodos()
        {
            return new List<usuario>();
        }


        public usuario ObterPorEmail(string email)
        {
            try
            {
                usuario usuario = (from user in _repositorio.usuario
                                   where user.email == email
                                   select user).FirstOrDefault();

                return usuario;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public usuario ObterPorEmailESenha(string email, string senha)
        {
            try
            {
                usuario usuario = (from user in _repositorio.usuario
                                   where user.email == email && user.senha == senha
                                   select user).FirstOrDefault();

                return usuario;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool Editar(usuario usuario)
        {
            try
            {
                _repositorio.ApplyCurrentValues(usuario.EntityKey.EntitySetName, usuario);
                _repositorio.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool Excluir(usuario usuario)
        {
            try
            {
               List<pet> listaPets = (from pets in _repositorio.pet
                                   where pets.usuario_id == usuario.id
                                   select pets).ToList();

               endereco endereco = (from end in _repositorio.endereco
                                    where end.id == usuario.endereco.id
                                    select end).FirstOrDefault();

               foreach (var item in listaPets)
               {
                   foto foto = (from f in _repositorio.foto
                                where f.id == item.foto_id
                                select f).FirstOrDefault();

                   _repositorio.DeleteObject(foto);
                   _repositorio.DeleteObject(item);
               }
               _repositorio.DeleteObject(endereco);
                _repositorio.DeleteObject(usuario);
                _repositorio.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public cidade ObterCidadePorId(int id)
        {
            try 
            {
                cidade cidade = (from city in _repositorio.cidade
                                 where city.id == id
                                 select city).FirstOrDefault();

                return cidade;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<usuario> ObterTodos()
        {
            try
            {
                List<usuario> listaUsuarios = (from usuarios in _repositorio.usuario
                                 select usuarios).ToList();

                return listaUsuarios;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<dica> ObterDicas()
        {
            try
            {
                List<dica> listaDicas = (from dicas in _repositorio.dica
                                               select dicas).ToList();

                return listaDicas;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public dica EnviarDica(dica dica)
        {
            try
            {
                _repositorio.AddTodica(dica);
                _repositorio.SaveChanges();
                

                return dica;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool ExcluirDica(dica dica)
        {
            try
            {
                _repositorio.DeleteObject(dica);
                _repositorio.SaveChanges();


                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public dica ValidarDica(dica dica)
        {
            try
            {
                _repositorio.ApplyCurrentValues(dica.EntityKey.EntitySetName, dica);
                _repositorio.SaveChanges();

                return dica;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public dica ObterDicaPorGuid(string guid)
        {
            try
            {
                dica dica = (from dicas in _repositorio.dica
                                     where dicas.guid == guid
                                     select dicas).FirstOrDefault();


                return dica;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
