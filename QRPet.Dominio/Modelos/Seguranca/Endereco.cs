﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QRPet.Dominio.Comum;


namespace QRPet.Dominio.Modelos.Seguranca
{
    public class Endereco : EntidadeBase
    {
        public string CEP { get; set; }
        public string Rua { get; set; }
        public string Bairro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }

        public Endereco()
        {
        }
    }
}
