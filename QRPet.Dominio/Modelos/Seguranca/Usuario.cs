﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QRPet.Dominio.Comum;


namespace QRPet.Dominio.Modelos.Seguranca
{
    public class Usuario: EntidadeBase
    {
        public string Nome { get; set; }
        public string Sexo { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public Endereco Endereco { get; set; }

        public Usuario()
        {
        }
    }
}
