﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QRPet.Dominio.Comum;

namespace QRPet.Dominio.Modelos.Seguranca
{
    public interface IRepositorioUsuario: IRepositorio<Usuario>
    {
        Usuario ObterPorLoginESenha(string email, string senha);

    }
}
