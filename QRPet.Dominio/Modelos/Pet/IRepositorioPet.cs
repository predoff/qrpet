﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QRPet.Dominio.Comum;

namespace QRPet.Dominio.Modelos.Pet
{
    public interface IRepositorioPet: IRepositorio<Pet>
    {
        List<Pet> ListarTodosOsPetsDoUsuario(int idUsuario);
    }
}
